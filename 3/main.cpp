#include <string>
#include <cmath>
#include <vector>
#include <iostream>

#include "../CImg.h"
#include <fftw3.h>

using namespace std;
using namespace cimg_library;

#define PAD_X_AXIS 0
#define PAD_Y_AXIS 1

typedef CImg<double> Img;
typedef vector<double> Kernel1D;

double gauss(double x, double mu, double sigma) {
    double a = (x - mu) / sigma;
    return exp(-0.5 * pow(a, 2));
}

Kernel1D getKernel1D(int N, double sigma) {
    Kernel1D kernel1d(N);
    int radius = floor(N / 2);

    double sum = 0.0;
    for (int i = 0; i < N; i++) {
        kernel1d[i] = gauss(i, radius, sigma);
        sum += kernel1d[i];
    }
    // Normalize
    for (int i = 0; i < N; i++) {
        kernel1d[i] /= sum;
    }
    return kernel1d;
}

void Conv2D(const string &imgName, double sigma) {
    Img img(("images/" + imgName + ".png").c_str());
    
    int height = img._height, width = img._width;

    Img conv_img(width, height);
    Img conv_img2(width, height);
    int kernel_size = 6.0 * sigma + 1;
    int radius = (kernel_size - 1) / 2;
    auto kernel1d = getKernel1D(kernel_size, sigma);
    cout << "Kernel size: " << kernel_size << ", radius: " << radius << endl;

    // X axis
    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            for (int k = -radius; k <= radius; k++) {
                if (x + k < 0) {
                    conv_img(x, y) += kernel1d[radius + k] * img(0, y);
                } else if (x + k > width) {
                    conv_img(x, y) += kernel1d[radius + k] * img(width - 1, y);
                } else {
                    conv_img(x, y) += kernel1d[radius + k] * img(x + k, y);
                }
            }
        }
    }
    
    // Y axis
    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            for (int k = -radius; k <= radius; k++) {
                if (y + k < 0) {
                    conv_img2(x, y) += kernel1d[radius + k] * conv_img(x, 0);
                } else if (y + k > height) {
                    conv_img2(x, y) += kernel1d[radius + k] * conv_img(x, height - 1);
                } else {
                    conv_img2(x, y) += kernel1d[radius + k] * conv_img(x, y + k);
                }
            }
        }
    }
    
    conv_img2.save(("results/" + imgName + "_kernel_" + to_string(kernel_size) + "_sigma_" + to_string(int(sigma)) + ".png").c_str());
}

int main(int argc, char *argv[]) {
    Conv2D(argv[1], stoi(argv[2]));
    return 0;
}
