g++ -std=c++17 -o main main.cpp -I "/opt/X11/include" -I "/usr/local/include" -L/opt/X11/lib -L/usr/local/lib -lX11 || exit 1

files=("dog" "baboon")
sigmas=("1" "2" "5" "10" "20")

for file in "${files[@]}"
do
    for sigma in "${sigmas[@]}"
    do
        bash -c "./main $file $sigma"
    done
done

echo "Done!"
