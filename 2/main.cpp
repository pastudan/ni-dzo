#include <string>
#include <cmath>
#include <vector>

#include "../CImg.h"
#include <fftw3.h>

using namespace std;
using namespace cimg_library;

typedef CImg<double> Img;

void fft(const string &imgName) {
    Img img(("images/" + imgName + ".png").c_str());
    img = img.get_normalize(0, 1);

    int height = img._height, width = img._width;
    fftw_complex *in;
    fftw_complex *out;
    fftw_complex *recovered;
    fftw_plan p_forward, p_backward;

    in = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * height * width);
    out = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * height * width);
    recovered = (fftw_complex *) fftw_malloc(sizeof(fftw_complex) * height * width);

    p_forward = fftw_plan_dft_2d(height, width, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
    p_backward = fftw_plan_dft_2d(height, width, out, recovered, FFTW_BACKWARD, FFTW_ESTIMATE);

    // Initialize
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            in[height * i + j][0] = (double) img(i, j);
            in[height * i + j][1] = 0;
            out[height * i + j][0] = 0;
            out[height * i + j][1] = 0;
            recovered[height * i + j][0] = 0;
            recovered[height * i + j][1] = 0;
        }
    }

    // Run plans
    fftw_execute(p_forward);

    vector<double> buffer(height * width);
    Img result(width, height, {0});
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            double real = out[height * i + j][0];
            double imaginary = out[height * i + j][1];
            auto magnitude = sqrt(pow(real, 2) + pow(imaginary, 2));
            buffer[height * i + j] = log(1.0 + magnitude);  // apply log
        }
    }

    int cx = width / 2;
    int cy = height / 2;

    // Swap quadrants for visualization
    for (int i = 0; i < cy; i++) for (int j = 0; j < cx; ++j) result(i, j) = buffer[height * (i + cy) + (j + cx)];
    for (int i = 0; i < cy; i++) for (int j = cx; j < width; ++j) result(i, j) = buffer[height * (i + cy) + (j - cx)];
    for (int i = cy; i < height; i++) for (int j = 0; j < cx; ++j) result(i, j) = buffer[height * (i - cy) + (j + cx)];
    for (int i = cy; i < height; i++) for (int j = cx; j < width; ++j) result(i, j) = buffer[height * (i - cy) + (j - cx)];

    fftw_execute(p_backward);

    Img reconstruction(width, height);
    for (int i = 0; i < height; ++i) {
        for (int j = 0; j < width; ++j) {
            reconstruction(i, j) = recovered[height * i + j][0];
        }
    }

    result = result.get_normalize(0, 255);
    result.save(("output/" + imgName + "_fft.png").c_str());
    reconstruction = reconstruction.get_normalize(0, 255);
    reconstruction.save(("output/" + imgName + "_reconstruction.png").c_str());

    fftw_destroy_plan(p_forward);
    fftw_destroy_plan(p_backward);
    fftw_free(in);
    fftw_free(out);
    fftw_free(recovered);
}

int main() {
    fft("baboon");
    fft("lena");
    fft("color");
    fft("stp1");
    fft("square");
    fft("circle");
    fft("small_square");
    fft("c");
    return 0;
}
