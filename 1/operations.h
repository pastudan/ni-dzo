//
// Created by Daniil Pastukhov on 27.02.2022.
//

#ifndef INC_1_OPERATIONS_H
#define INC_1_OPERATIONS_H

#include <iostream>
#include <algorithm>
#include <vector>
#include <numeric>
#include "../CImg.h"

using namespace std;
using namespace cimg_library;

typedef CImg<double> Img;
const int BINS_NUMBER = 256;

Img imgLike(const Img &img) {
    return Img(img._width, img._height, img._depth, img._spectrum, 0);
}

namespace operations {
    Img negative(const Img &img) {
        auto newImg = imgLike(img);
        cimg_forXYZC(img, x, y, z, c) {
                        // v' = 255 - v.
                        newImg(x, y, z, c) = 255 - img(x, y, z, c);
                    }
        return newImg;
    }

    /*
     * Apply threshold
     */
    Img threshold(const Img &img, uint8_t threshold) {
        auto newImg = imgLike(img);
        cimg_forXYZC(img, x, y, z, c) {
                        // v' = 0 if v < threshold, 1 otherwise
                        newImg(x, y, z, c) = 255 * (img(x, y, z, c) >= threshold);
                    }
        return newImg;
    }

    Img changeBrightness(const Img &img, int8_t alpha) {
        auto newImg = imgLike(img);
        if (abs(alpha) > 255) {
            cerr << "Alpha is not in range [-255, 255]" << endl;
            return newImg;
        }
        cimg_forXYZC(img, x, y, z, c) {
                        // v' = v + alpha (v' is always bounded to [0, 255])
                        newImg(x, y, z, c) = clamp((double) img(x, y, z, c) + alpha, 0.0, 255.0);
                    }
        return newImg;
    }

    Img changeContrastByConstant(const Img &img, float alpha) {
        auto newImg = imgLike(img);
        if (alpha < 0) {
            cerr << "Alpha is not in range [0, +inf]" << endl;
            return newImg;
        }
        cimg_forXYZC(img, x, y, z, c) {
                        // v' = alpha * v (v' is always bounded to [0, 255])
                        newImg(x, y, z, c) = clamp((double) alpha * img(x, y, z, c), 0.0, 255.0);
                    }
        return newImg;
    }

    Img changeContrast(const Img &img, float factor) {
        auto newImg = imgLike(img);
        cimg_forXYZC(img, x, y, z, c) {
                        // v' = alpha * (v - 128) + 128 (v' is always bounded to [0, 255])
                        newImg(x, y, z, c) = clamp(factor * (img(x, y, z, c) - 128.0) + 128.0, 0.0, 255.0);
                    }
        return newImg;
    }

    Img gammaCorrection(const Img &img, float gamma) {
        auto newImg = imgLike(img);
        newImg = newImg.get_normalize(0.0, 1.0);
        cimg_forXYZC(img, x, y, z, c) {
                        // v' = v^(gamma) (v' is always bounded to [0, 1])
                        newImg(x, y, z, c) = pow(img(x, y, z, c), gamma);
                    }
        newImg = newImg.get_normalize(0, 255);
        return newImg;
    }

    Img equalizeHist(const Img &img) {
        auto newImg = imgLike(img);

        // compute histogram
        vector<double> hist(BINS_NUMBER);
        cimg_forXYZC(img, x, y, z, c) {
                        hist[img(x, y, z, c)]++;
                    }

        // calculate mapping
        auto total = img.size();
        auto cumSum = 0.0;
        vector<unsigned char> mapping(BINS_NUMBER);
        for (int i = 0; i < BINS_NUMBER; ++i) {
            cumSum += hist[i];
            mapping[i] = (255.0 * cumSum) / total;
        }

        // equalize
        cimg_forXYZC(img, x, y, z, c) {
                        newImg(x, y, z, c) = mapping[img(x, y, z, c)];
                    }
        return newImg;
    }
};


#endif //INC_1_OPERATIONS_H
