#include "../CImg.h"
#include "operations.h"

using namespace operations;


int main() {
    const string IMG_PATH = "images/dog.jpg";
    Img img(IMG_PATH.c_str());

    // apply operations
    auto contrastImg = changeContrast(img, 2.0);
    auto negativeImg = negative(img);
    auto thresholdImg = threshold(img, 128);
    auto brightImg = changeBrightness(img, 50);
    auto gammaImg = gammaCorrection(img, 0.5);
    auto eqImg = equalizeHist(img);

    // save images to 'output' folder
    img.save("output/original.bmp");
    contrastImg.save("output/contrast.bmp");
    negativeImg.save("output/negative.bmp");
    thresholdImg.save("output/threshold.bmp");
    brightImg.save("output/brightness.bmp");
    gammaImg.save("output/gamma.bmp");
    eqImg.save("output/eq.bmp");

    return 0;
}
