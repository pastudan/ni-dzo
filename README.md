# Tech stack
- C++17
- CImg library [https://cimg.eu/](https://cimg.eu/) (HW1, 2)
- FFTW3 library (HW2)

# HW1
Compile and run on MacOS:
```bash
cd 1
g++ -std=c++17 main.cpp -o main -I "/opt/X11/include" -L/opt/X11/lib -lX11 && ./main
```

# HW2
Compile and run on MacOS:
```bash
cd 2
g++ -std=c++17 -o main main.cpp -I "/opt/X11/include" -I "/usr/local/include" -L/opt/X11/lib -L/usr/local/lib -lX11 -lfftw3 && ./main
```

# HW3
Compile and run on MacOS:
```bash
cd 3 && bash run.sh
```