g++ -std=c++17 -o main main.cpp -I "/opt/X11/include" -I "/usr/local/include" -L/opt/X11/lib -L/usr/local/lib -lX11 || exit 1

files=("lena" "baboon" "dog")
sigmas=("1" "2" "5" "10" "20")
sigmas2=("1" "5" "10" "20")

for file in "${files[@]}"
do
    for sigma in "${sigmas[@]}"
    do
        for sigma2 in "${sigmas2[@]}"
        do
            bash -c "./main $file $sigma $sigma2"
        done
    done
done

echo "Done!"
