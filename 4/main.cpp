#include <string>
#include <cmath>
#include <vector>
#include <iostream>

#include "../CImg.h"
#include <fftw3.h>

using namespace std;
using namespace cimg_library;

#define PAD_X_AXIS 0
#define PAD_Y_AXIS 1

typedef CImg<double> Img;
typedef vector<double> Kernel1D;
typedef vector<Kernel1D> Kernel2D;

double gauss(double x, double mu, double sigma) {
    double a = (x - mu) / sigma;
    return exp(-0.5 * pow(a, 2));
}

Kernel1D getKernel1D(int N, double mu, double sigma) {
    Kernel1D kernel1d(N);

    double sum = 0.0;
    for (int i = 0; i < N; i++) {
        kernel1d[i] = gauss(i, mu, sigma);
        sum += kernel1d[i];
    }
    // Normalize
    for (int i = 0; i < N; i++) kernel1d[i] /= sum;
    return kernel1d;
}

Kernel2D getKernel2D(int N, double mu, double sigma) {
    auto kernel1d = getKernel1D(N, mu, sigma);
    Kernel2D kernel2d(N);
    for (auto &el: kernel2d) el.reserve(N);

    for (int i = 0; i < N; i++)
        for (int j = 0; j < N; j++)
            kernel2d[i][j] = kernel1d[i] * kernel1d[j];

    return kernel2d;
}

void Conv2D(const string &imgName, double sigma, double sigma2) {
    Img img(("images/" + imgName + ".png").c_str());
    
    int height = img._height, width = img._width;

    Img conv_img(width, height);
    int kernel_size = 6.0 * sigma + 1;
    int radius = (kernel_size - 1) / 2;
    auto kernel2d = getKernel2D(kernel_size, radius, sigma);

    cout << "Kernel size: " << kernel_size << ", radius: " << radius << endl;

    // X axis
    for (int x = 0; x < width; x++) {
        for (int y = 0; y < height; y++) {
            double W = 0.0;
            for (int k = -radius; k <= radius; k++) {
                for (int l = -radius; l <= radius; l++) {
                    double norm = 0.0;
                    if (x + k < 0) {
                        norm = kernel2d[radius + k][radius + l] * gauss(img(0, 0) - img(x, y), 0, sigma2);
                        conv_img(x, y) += norm * img(0, y);
                    } else if (y + l < 0) {
                        norm = kernel2d[radius + k][radius + l] * gauss(img(x, height - 1) - img(x, y), 0, sigma2);
                        conv_img(x, y) += norm * img(x, height - 1);
                    } else if (x + k >= width) {
                        norm = kernel2d[radius + k][radius + l] * gauss(img(width - 1, y) - img(x, y), 0, sigma2);
                        conv_img(x, y) += norm * img(width - 1, y);
                    } else if (y + l >= height) {
                        norm = kernel2d[radius + l][radius + l] * gauss(img(x, height - 1) - img(x, y), 0, sigma2);
                        conv_img(x, y) +=norm * img(x, height - 1);
                    } else {
                        norm = kernel2d[radius + k][radius + l] * gauss(img(x + k, y + l) - img(x, y), 0, sigma2);
                        conv_img(x, y) += norm * img(x + k, y + l);
                    }
                    W += norm;
                }
            }
            conv_img(x, y) /= W;
        }
    }
    
    conv_img.save(("results/" + imgName + "_n_" + to_string(kernel_size) + "_s1_" + to_string(int(sigma)) + "_s2_" + to_string(int(sigma2)) + ".png").c_str());
}

int main(int argc, char *argv[]) {
    Conv2D(argv[1], stoi(argv[2]), stoi(argv[3]));
    return 0;
}
